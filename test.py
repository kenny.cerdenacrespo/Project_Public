import sys
import datetime
import random
import string
import argparse
import json
import os

currentDirectory = os.getcwd ()

def password_generator(length=10):
    chars = string.ascii_letters + string.digits
    password = ""
    for number in range(0, length):
        char = random.choice(chars)
        password = password + char
    return password

def animals_generator():
    animals = ""
    with open(currentDirectory + '/animals.txt','r') as first_word:
        lignes1 = first_word.readlines()
        animals = random.choice(lignes1).strip()
 return animals

def adjectives_generator():
    adjectives = ""
    with open(currentDirectory + '/english-adjectives.txt','r') as second_word:
        lignes2 = second_word.readlines()
        adjectives = random.choice(lignes2).strip()
 return adjectives

def user_generator():
    login = {
            "username" : animals_generator() + adjectives_generator(),
            "password" : password_generator(length)
            }
    return login

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--length", help = "Defini la longueur du Mdp", action="store", type=int, default=10)
    parser.add_argument("-u", "--user", help = "Defini le nombre de user", action="store", type=int, default=1)
    args = parser.parse_args()


    user = args.user
    length = args.length

    all_logins = []
    for i in range(0, args.user):
        login = user_generator()
        all_logins.append(login)

    print(all_logins)

    with open ("data_file.json", "r") as user_file:
        data = json.load(user_file)

    with open ("data_file.json", "w") as user_file:
         json.dump(all_logins, user_file)
exit()
